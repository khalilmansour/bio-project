# Testing for correlation between the seed count and height of plants in A & B genotypes

The purpose of this project is to programatically look for correlations in a large dataset using statistical tests.

## Inputs

- 3 Datasets
  - a_seeds.txt: Contains the seed counts for each individual of genotype A.
  - b_seeds.txt: Contains the seed counts for each individual of genotype B.
  - heights.txt: Contains the heights for each individual of both genotypes.

## Output

- Merged `.txt` file containing all the information stored in the input files.
- Box plots showing the height and seed count for each genotype.
- Scatter plots showing the relation between seed count and height.
- Statistical tests to determine if there is a positive correlation between seed count and height.

## Pseudocode

### Define Key Variables

### Define Key Functions

### Define Program Structure

### Statistical Tests

#### Hypothesis

If there is a significant linear relationship between the independent variable X and the dependent variable Y, the slope will not equal zero.

**Ho: Β1 = 0**

**Ha: Β1 ≠ 0**

#### Hypothesis Test for Regression Slope

##### Significance level

For this analysis, the significance level is 0.05. Using the sample data, we will conduct a linear regression t-test to determine whether the slope of the regression line differs significantly from zero.

#### Sample analysis for genotype A

Coefficient of determination:
Standard error of slope:
Slope of the regression line:
Degrees of freedom:
Test statistic:
P-value of test statistic:

#### Sample analysis for genotype B

Coefficient of determination:
Standard error of slope:
Slope of the regression line:
Degrees of freedom:
Test statistic:
P-value of test statistic:
