# imports 
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from scipy import stats
from scipy.special import stdtr

# main function
def main():
  # init dictionaries
  a_dict, b_dict, h_dict = {}, {}, {}

  # read files into dictionaries
  create_dictionary('a_seeds.txt', a_dict)
  create_dictionary('b_seeds.txt', b_dict)
  create_dictionary('heights.txt', h_dict)

  # merge dictionaries
  result = merge(a_dict, b_dict, h_dict)

  # filter dictionary
  data = clean(result)

  # result is a dictionary with keys as tuple (Individual, Genotype) and values as tuple (Seed Count, Height)
  write(data)

  # generate boxplots for height and seed count
  boxplot(data)

  # generate a scatter plot with data from both genotypes
  scatterplot(data)

  # perform t-tests for both the seed counts and the heights
  t_tests(data)

  # perform a pearson r correlation to determine strenght of relationship between height and seed count
  pearson_correlation(data)

# this function takes all three dictionaries as parameters and merges them into one single dictionary
def merge(a_dict, b_dict, h_dict):
  # new dict
  result = {}
  # merge seed dictionaries into one (works with python 3 and up)
  seeds = {**a_dict, **b_dict}
  # create an array containing both the seeds and heights dictionaries 
  dict_array = [seeds, h_dict]
  # merge seeds and heights
  for k in seeds.keys():
    try:
      # fetches both values with same key from each dict in the array and creates a tuple
      result[k] = tuple(result[k] for result in dict_array)
    except KeyError:
      print('Invalid key match detected: {0}. Removing entry from data. No match has been found for this key.'.format(k))
      continue
  
  return result

# this function writes the dictionary into a new data file
def write(result):
  # open file
  with open('data.txt', 'w') as data:
    # write values
    for key, val in result.items():
      data.write('{0}\t{1}\t{2}\t{3}\n'.format(key[1], key[0], val[0], val[1]))

# this function creates dictionaries from input files
def create_dictionary(file_name, dictionary):
  with open(file_name, 'r') as data:
    # skip header
    data = iter(data)
    next(data)
    if file_name == 'heights.txt':
      for line in data:
        (individual, genotype, height) = line.split()
        dictionary[(int(individual), genotype)] = height
    else:
      for line in data:
        (genotype, individual, seed_count) = line.split()
        dictionary[(int(individual), genotype)] = seed_count

# this function removes entries with invalid values
def clean(result):
  res = {}
  for key, value in result.items():

    # name variables
    individual = key[0]
    genotype = key[1]
    seed_count = value[0]
    height = value[1]

    # validate individual
    try:
      int(individual)
      if individual < 1:
        print('A non positive integer Individual number has been filtered out of the dataset: {0}'.format(individual))
        continue
    except ValueError:
      print('A non positive integer Individual number has been filtered out of the dataset: {0}.'.format(individual))
      continue

    # validate genotype
    if genotype not in ['A', 'B']:
      print('Incorrect Genotype value has been filtered out of the dataset: {0}.'.format(genotype))
      continue

    # validate values
    try:
      # cast from string to float in order to manipulate
      seed_count_float = float(seed_count)
      height_float = float(height)

      if not seed_count_float.is_integer():
        print('A non positive integer seed count has been filtered out of the dataset: {0}'.format(seed_count))
        continue
      elif seed_count_float < 1.0:
        print('A non positive integer seed count has been filtered out of the dataset: {0}'.format(seed_count))
        continue
      elif height_float <= 0:
        print('Negative or null height value has been filtered out of the dataset: {0}'.format(height))
        continue
    except ValueError:
      print('Entry containing the following incorrect values has been filtered out of the dataset: ({0}, {1})'.format(seed_count, height))
      continue
  
    res[key] = value

  print('\n')
  return res

# this function takes the data dict and extracts the values into an array of arrays for plotting
def prepare_plots(data):
  # init arrays
  a_seed_count_array = []
  b_seed_count_array = []
  a_height_array = []
  b_height_array = []

  for key, val in data.items():
    if key[1] == 'A':
      a_seed_count_array.append(float(val[0]))
      a_height_array.append(float(val[1]))
    else:
      b_seed_count_array.append(float(val[0]))
      b_height_array.append(float(val[1]))
  
  return [a_seed_count_array, b_seed_count_array, a_height_array, b_height_array]

# this function plots the heights and seed counts of each Genotype in boxplot format
def boxplot(data):

  x = prepare_plots(data)

  # merge arrays
  seed_counts = [x[0], x[1]]
  heights = [x[2], x[3]]

  # create two subplots horizontally stacked
  fig, (ax1, ax2) = plt.subplots(1, 2)

  fig.set_size_inches(15,7)

  # plot data
  bp1 = ax1.boxplot(seed_counts, patch_artist = True, notch=True)
  bp2 = ax2.boxplot(heights, patch_artist = True, notch=True)

  # show grids
  [ax.grid() for ax in [ax1, ax2]]

  # define colors
  colors = ['#b3b3ff', '#ffb3b3']

  # aesthetic changes to plots
  for bp in [bp1, bp2]:
    for patch, color in zip(bp['boxes'], colors):
      patch.set_facecolor(color)

    # changing color and linewidth of whiskers 
    for whisker in bp['whiskers']:
      whisker.set(linewidth = 1, linestyle ="--")

    # changing color and linewidth of medians
    for median in bp['medians']:
      median.set(color = 'black', linestyle=':')

    # changing style of flyers
    for flier in bp['fliers']:
      flier.set(marker = 'X', alpha = 0.5)

  # set titles
  ax1.set_title('Spread and centers of seed count')
  ax2.set_title('Spread and centers of height')

  # set y-labels
  ax1.set_ylabel('Seed count')
  ax2.set_ylabel('Height')

  # define legend colors
  blue_patch = mpatches.Patch(color='#b3b3ff', label='Genotype A')
  red_patch = mpatches.Patch(color='#ffb3b3', label='Genotype B')

  # turn off ticks for x-axis and add legend to plot
  for ax in (ax1, ax2):
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax.legend(handles=[blue_patch, red_patch], loc='lower right', bbox_to_anchor=(1, -0.12))

  plt.show()

# this function plots the individual heights by individual seed counts
def scatterplot(data):

  x = prepare_plots(data)

  # name variables
  a_seed_count_array = x[0]
  b_seed_count_array = x[1]
  a_height_array = x[2]
  b_height_array = x[3]

  # create two subplots and share axes
  fig = plt.figure(figsize=(15, 7))

  # show grid
  plt.grid()

  # plot data
  plt.scatter(a_seed_count_array, a_height_array, color='#9999ff', marker='.')
  plt.scatter(b_seed_count_array, b_height_array, color='#ff9999', marker='.')

  # merge genotypes
  seed_count = a_seed_count_array + b_seed_count_array
  heights = a_height_array + b_height_array

  # perform linear regression on scatter plot
  linear_regression(seed_count, heights, plt)

  # set labels for axes
  plt.ylabel('Height')
  plt.xlabel('Seed Count')

  plt.title('Seed heights according to seed counts for both genotypes')

  # add legend
  blue_patch = mpatches.Patch(color='#b3b3ff', label='Genotype A')
  red_patch = mpatches.Patch(color='#ffb3b3', label='Genotype B')
  plt.legend(handles=[blue_patch, red_patch], loc='best')

  plt.show()

# this functions plots the linear regression
def linear_regression(x, y, graph):
  # get slope (m) and intercept (b) using ordinary least squares method
  p, res, _, _, _ = np.polyfit(x, y, 1, full=True)

  # name variables
  m = p[0]
  b = p[1]

  # transform array into numpy array for plotting
  x = np.array(x)
  # add linear regression line to scatter plot
  graph.plot(x, m*x + b, linestyle='--')

# this function performs two-sample t-tests to find out if population means of two groups are equal or not
def t_tests(data):

  x = prepare_plots(data)

  # name variables
  a_seed_count_array = np.array(x[0])
  b_seed_count_array = np.array(x[1])
  a_height_array = np.array(x[2])
  b_height_array = np.array(x[3])

  # create array of arrays for seed count and height
  values = [[a_seed_count_array, b_seed_count_array], [a_height_array, b_height_array]]

  # seeds OR height for output printing
  seeds = True

  # iterate the array of arrays
  for x in values:
    if seeds:
      print('T-test for seed count on genotypes A & B\n----------')
      val = 'seed count'
    else:
      print('T-test for height on Genotypes A & B\n----------')
      val = 'height'

    # name variables
    a = x[0]
    b = x[1]

    # lengths
    N_A = len(a)
    N_B = len(b)

    # variances (ddof = 1 for N-1)
    var_a = a.var(ddof=1)
    var_b = b.var(ddof=1)

    # calculate the t-statistic
    t = (a.mean() - b.mean())/(np.sqrt(var_a/N_A + var_b/N_B))
    
    # degrees of freedom (not assuming equal variances)
    df = np.square(var_a/N_A + var_b/N_B)/(np.square(var_a/N_A)/(N_A-1) + np.square(var_b/N_B)/(N_B-1))

    # significance level (alpha)
    significance_level = 0.05

    # p-value
    p = 2 * stdtr(df, -np.abs(t))

    # cross checking with the internal scipy function
    t2, p2 = stats.ttest_ind(x[0], x[1], equal_var=False)

    # t critical
    t_crit = stats.t.ppf(q=1-.05/2, df=df)

    # evaluate with t critical value
    reject = True
    if -t_crit < t < t_crit:
      reject = False

    # print results
    print('H0: The population means for both distributions are equal (null hypothesis).')
    print('Variance A: {}\nVariance B: {}'.format(var_a, var_b))
    print('Significance level: {}'.format(significance_level))
    print('Degree of freedom: {}'.format(df))
    print('Test statistic (from formula): {}'.format(t))
    print('p (from formula): {}'.format(p))
    print("Test statistic (from scipy): {}".format(t2))
    print("p (from scipy): {}".format(p2))
    print('t critical: {}\n'.format(t_crit))

    if p == p2 and t == t2:
      print('Results from the formula match the results from scipy.\n')

      if p < significance_level and reject:
          print('Since p < alpha (0.05) and the t statistic is not in between {} and {}, we reject Ho and therefore there is a significant difference between both distributions.\nWe can conclude that genotype has an effect on {}.\n'.format(-t_crit, t_crit, val))
      else:
          print('Since p > alpha (0.05), and the t statistic is between {} and {}, we fail to reject Ho and therefore there is no significant difference between the distributions.\nWe can conclude that genotype has no effect on.\n'.format(-t_crit, t_crit, val))
    seeds = False

# this function calculates the pearson's correlation coefficient to determine the strenght of the relationship between the variables
def pearson_correlation(data):

  x = prepare_plots(data)

  # name variables
  seed_count = np.array(x[0] + x[1])
  height = np.array(x[2] + x[3])
  significance_level = 0.05

  # calculate correlation coefficient and p value
  coeff, p = stats.pearsonr(seed_count, height)

  # print results
  print('Pearson correlation between seed count and height\n----------')
  print('H0: The correlation coefficient is 0 (null hypothesis).')
  print('Significance level: {}'.format(significance_level))
  print('The pearson correlation coefficient is: {}'.format(coeff))
  print('The p value is: {}'.format(p))

  # interpret p value
  if p < significance_level:
    print('Since p < alpha (0.05), we reject H0 and therefore we can conclude that the correlation coefficient is statistically significant.\n')
  else:
    print('Since p > alpha (0.05), we fail to reject H0 and therefore we can conclude that the correlation coefficient is statistically insignificant.\n')

  # interpret coefficient value
  if coeff > 0:
    print('A positive coefficient means that the correlation between the two variables is positive.')
  else:
    print('A negative coefficient means that the correlation between the two variables is negative.')

main()
